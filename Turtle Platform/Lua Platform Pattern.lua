local p = peripheral.wrap("front")
local w = 1
local d = 2
local l = 1
local c = 1
local x = 1
local arg = { ... }
local currentSlot
local chest = {[1] = "1365", [2] = "2992", [3] = "2998", [4] = "3003", [5] = "3007", [6] = "2993", [7] = "2994"}
 
if (#arg ~= 2) then
  error("Usage: "..shell.getRunningProgram().." <pattern> <depth>")
end
 
local platformSize = {tonumber(arg[w]), tonumber(arg[d])}
--print("Default x: "..x)
x = {tonumber(arg[x])}
x = x[1]
--print("new x: "..x[1])
 
if (l % 2 == 0) then
  even = 1
else
  even = 0
end
 
function diggingMoveForward()
  if (turtle.detect()) then
    turtle.dig()
  end
  turtle.forward()
  if (turtle.detectUp()) then
    turtle.digUp()
  end
end
 
function moveRight()
  turtle.turnRight()
  diggingMoveForward()
  turtle.turnRight()
end
 
function moveLeft()
  turtle.turnLeft()
  diggingMoveForward()
  turtle.turnLeft()
end
 
function turnReverse()
  turtle.turnLeft()
  turtle.turnLeft()
end
 
function blockPlace()
  while (turtle.getItemCount(currentSlot) < 1) do
    currentSlot = currentSlot + 1
    if currentSlot == 16 then
      restock()
    end
  end
  turtle.select(currentSlot)
  if (turtle.detectDown()) then
    if (turtle.compareDown()) ~= true then
      turtle.digDown()
    end
  end
  turtle.placeDown()
end
 
function build()
  for row = 1, l, 1 do
    turtle.select(currentSlot)
    for spot = 1, platformSize[d], 1 do
      blockPlace()
      if (spot < platformSize[d]) then
        diggingMoveForward()
      end
    end
    if (row < l) then
      break
    end
  end
end

function stairPlace()
  while (turtle.getItemCount(currentSlot) < 1) do
    currentSlot = currentSlot + 1
    if currentSlot == 16 then
      restock()
    end
  end
  turtle.select(currentSlot)
  if (turtle.detectDown()) then
    if (turtle.compareDown()) ~= true then
      turtle.digDown()
    end
  end
  turtle.turnRight()
  turtle.placeDown()
  turtle.turnLeft()
end
 
function stair()
  for row = 1, l, 1 do
    turtle.select(currentSlot)
    for spot = 1, platformSize[d], 1 do
      stairPlace()
      if (spot < platformSize[d]) then
        diggingMoveForward()
      end
    end
    if (row < l) then
      break
    end
  end
end
 
function turtleReturn()
  if (even ~= 1) then
    turnReverse()
    for spot = 1, (platformSize[d]-1), 1 do
      turtle.forward()
    end
  end
  turtle.turnRight()
  for spot = 1, (l-1), 1 do
    turtle.forward()
  end
  turtle.turnLeft()
  turtle.forward()
  turnReverse()
end
 
function restock()
  turnReverse()
  turtle.select(16)
  turtle.place()
  p = peripheral.wrap("front")
  p.setFrequency(chest[1])
  for i=1,15 do
    turtle.select(i)
    turtle.drop()
  end
  p.setFrequency(chest[c])
  for i=1,15 do
    turtle.select(i)
    turtle.suck()
  end
  turtle.select(16)
  turtle.dig()
  currentSlot = 1
  turtle.select(currentSlot)
  turnReverse()
end

function empty()
  turnReverse()
  turtle.select(16)
  turtle.place()
  p = peripheral.wrap("front")
  p.setFrequency(chest[1])
  for i=1,15 do
    turtle.select(i)
    turtle.drop()
  end
  turtle.select(16)
  turtle.dig()
  currentSlot = 1
  turtle.select(currentSlot)
  turnReverse()
end
 
currentSlot = 1
turtle.select(currentSlot)
diggingMoveForward()
 
function p1()
  l = 5
  print("Pattern: 2, Area: W = 5, D = "..platformSize[d])
  c = 5
  restock()
  build()
  c = 2
  restock()
  turtle.up()
  turnReverse()
  build()
  c = 3
  restock()
  turtle.turnLeft()
  turtle.forward()
  turtle.down()
  turtle.turnLeft()
  build()
  c = 4
  restock()
  moveRight()
  build()
  c = 3
  restock()
  moveLeft()
  build()
  c = 5
  restock()
  moveRight()
  build()
  c = 2
  restock()
  turtle.up()
  turnReverse()
  build()
  empty()
  turtleReturn()
  turtle.down()
end
 
function p2()
  l = 5
  print("Pattern: 2, Area: W = 5, D = "..platformSize[d])
  c = 6
  restock()
  stair()
  c = 7
  restock()
  turtle.up()
  turnReverse()
  build()
  c = 5
  restock()
  turtle.turnLeft()
  turtle.forward()
  turtle.down()
  turtle.turnLeft()
  build()
  moveRight()
  build()
  moveLeft()
  build()
  c = 6
  restock()
  moveRight()
  stair()
  c = 7
  restock()
  turtle.up()
  turnReverse()
  build()
  empty()
  turtleReturn()
  turtle.down()
end
 
if x==1 then
 p1()
end
 
if x==2 then
 p2()
end