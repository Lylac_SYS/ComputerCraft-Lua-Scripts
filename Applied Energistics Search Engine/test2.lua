ae = peripheral.find("tileinterface")
vc = peripheral.wrap("bottom")
chestSize = vc.getInventorySize()
dir = "down"
num = 1411
txt = "BIGREACTOR"
words = {"request", 2, 1}
Bleh = {
  id = "Thaumcraft:WandCasting",
  dmg = 108,
  nbt_hash = "a4e35a0a7c2501f925f17ba07d666ad2"
}

function getRawInv()
  rawInv = ae.getAvailableItems()
  inv = {}
  for i = 1, #rawInv do 
    inv[i] = {}
    inv[i].id = rawInv[i].fingerprint.id
    inv[i].size = rawInv[i].size
    inv[i].dmg = rawInv[i].fingerprint.dmg
    inv[i].nbt = rawInv[i].fingerprint.nbt_hash
  end
  --if raw item has nbt
  if rawInv[num].fingerprint.nbt_hash then print("nbt: true") else print("nbt: false") end
  --print inv nbt
  textutils.pagedPrint(textutils.serialize(inv[num].nbt))
  print("rawInv:")
  textutils.pagedPrint(textutils.serialize(rawInv[num]))
  print("inv:")
  textutils.pagedPrint(textutils.serialize(inv[num]))
end

function search(txt)
  getRawInv()
  txt = string.lower(tostring(txt))
  print("Search Args: "..txt)
  matches = {}
  print("Begin Search")
  for i = 1, #inv do 
    ID = string.lower(inv[i].id)
    id = inv[i].id
    size = inv[i].size
    dmg = inv[i].dmg
    assert(size)
    assert(id)
    if string.find(ID, txt) then
      matches[#matches + 1] = {}
      matches[#matches].id = id
      matches[#matches].size = size
      matches[#matches].dmg = dmg
      if inv[i].nbt then
        nbt = inv[i].nbt
        assert(nbt)
        matches[#matches].nbt = nbt
      end
      textutils.pagedPrint("["..i.."]:["..matches[#matches].size.."]"..matches[#matches].id..":"..textutils.serialize(matches[#matches].nbt))
    end
  end
  print("Search Result:")
  --printed out search results
  textutils.pagedPrint(textutils.serialize(matches[words[2]]))
  print("End of Search")
  if #matches > 0 then
    return matches
  else
    return false
  end
end

function getItems(id, amount, dmg, nbt)
  if not dmg then dmg = 0 end
  if not amount then return false end
  if not id then return false end
  Item = {}
  Item.id = id
  Item.dmg = dmg
  if nbt then
    Item.nbt_hash = nbt
  end
  print("Get Item:")
  textutils.pagedPrint(textutils.serialize(Item))
  print("Detailed Item:")
  textutils.pagedPrint(textutils.serialize(ae.getItemDetail(Item, false)))
--  print("Proper Fingerprint:")
--  textutils.pagedPrint(textutils.serialize(Bleh))
  if amount <= 0 then
    return
  elseif amount > 0 and amount <= 64 then
    dump = amount
    pull = 1
    elseif amount > 64 then
    dump = 64
    pull = math.floor(amount/64)
    if (amount/64)-pull > 0 then
      pull = pull+1
    end
  end
  if pull <= chestSize then
    for i = 1, chestSize do 
      slot = vc.getStackInSlot(i)
      if (pull > 0) and (slot == nil) then
        ae.exportItem(Item, dir, dump)
        turtle.dropDown(64)
        pull = pull-1
      end
      if (i == chestSize) and (pull > 0) then
        print("The chest is full. Please come back later.")
        break
      end
    end
  else
    print("Requested Amount is too big!")
    print("Max Chest Size is "..chestSize.." Stacks!")
  end
end
search(txt)
getItems(matches[words[2]].id, words[3], matches[words[2]].dmg, matches[words[2]].nbt)
