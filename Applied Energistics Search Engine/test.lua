ae = peripheral.find("tileinterface")
vc = peripheral.wrap("bottom")
c2 = peripheral.wrap("left")
chestSize = vc.getInventorySize()
dir = "down"
 
function split(str, splitter)
  if not splitter then return end
  if not str then return end
  words = {}
  i = 0
  for part in string.gmatch(str, "[^%"..splitter.."]+") do
    i = i + 1
    words[i] = part
  end
  return words
end
string.split = split

function getRawInv()
  rawInv = ae.getAvailableItems()
  inv = {}
  for i = 1, #rawInv do
    inv[i] = {}
    inv[i].id = rawInv[i].fingerprint.id
    inv[i].size = rawInv[i].size
    inv[i].dmg = rawInv[i].fingerprint.dmg
    inv[i].nbt = rawInv[i].fingerprint.nbt_hash
  end
end
 
function search(txt)
  print("Searching...")
  getRawInv()
  txt = string.lower(tostring(txt))
  matches = {}
  for i = 1, #inv do
    ID = string.lower(inv[i].id)
    id = inv[i].id
    size = inv[i].size
    dmg = inv[i].dmg
    assert(id)
    assert(size)
    if string.find(ID, txt) then
      matches[#matches + 1] = {}
      matches[#matches].id = id
      matches[#matches].size = size
      matches[#matches].dmg = dmg
      if inv[i].nbt then
        nbt = inv[i].nbt
        assert(nbt)
        matches[#matches].nbt_hash = nbt
      end
--      textutils.pagedPrint(textutils.serialize(matches[#matches]))
--[[      ae.exportItem(matches[#matches], dir, 1)
--      textutils.pagedPrint(textutils.serialize(c2.getStackInSlot(1)))
      if c2.getStackInSlot(1) then
        matches[#matches].name = c2.getStackInSlot(1).display_name
      else
        matches[#matches].name = id
      end
      turtle.dropUp()]]
    end
  end
  if #matches > 0 then
    return matches
  else
    return false
  end
end

function getItems(id, amount, dmg, nbt)
  if not dmg then dmg = 0 end
  if not amount then return false end
  if not id then return false end
  Item = {}
  Item.id = id
  Item.dmg = dmg
  if nbt then
    Item.nbt_hash = nbt
  end
--  print("Get Item:")
--  textutils.pagedPrint(textutils.serialize(Item))
  if amount <= 0 then
    return
  elseif amount > 0 and amount <= 64 then
    dump = amount
    pull = 1
  elseif amount > 64 then
    dump = 64
    pull = (math.floor(amount/64))
    if (amount/64)-pull > 0 then
      pull = pull+1
    end
  end
  if pull <= chestSize then
    for i = 1, chestSize do
      slot = vc.getStackInSlot(i)
      if (pull > 0) and (slot == nil) then
        ae.exportItem(Item, dir, dump)
        turtle.dropDown(64)
        pull = pull - 1
      end
      if (i == chestSize) and (pull > 0) then
        print("The chest is full. Please come back later.")
        break
      end
    end
  else
    print("Requested Amount is too big!")
    print("Max Chest Size is "..chestSize.." Stacks!")
  end
end
 
while true do
  print("commands: search")
  term.write(">")
  commands = io.read()
  if not commands then break end
  commands = string.split(commands, " ")
  if string.lower(commands[1]) == "search" and commands[2] then
    matches = search(commands[2])
    if type(matches) == "table" then
      print(#matches," Search Results!")
      print("# : Amount : ID : Meta")
      for i = 1, #matches do 
        textutils.pagedPrint(i..":["..matches[i].size.."]"..matches[i].id.." : "..matches[i].dmg)
      end
    end
    textutils.pagedPrint("End of List!")
    print("# : Amount : ID : Meta")
    if matches then
      while true do 
        print("request <#> <Amount>")
        print("newList <#>")
        term.write(">")
        commands = io.read()
        if not commands then break end
        commands = string.split(commands, " ")
        print(textutils.serialize(commands[1]..commands[2]))--..commands[3]))
        if string.lower(commands[1]) == "request" and commands[2] and commands[3] then
          commands[2] = tonumber(commands[2])
          commands[3] = tonumber(commands[3])
          if type(words[2]) == "number" and type(words[3]) == "number" then
            if words[2] <= #matches and words[2] > 0 then
--              print("Search Result:")
--              textutils.pagedPrint(textutils.serialize(matches[words[2]]))
              getItems(matches[words[2]].id, words[3], matches[words[2]].dmg, matches[words[2]].nbt_hash)
              break
            else
              print("Wrong Input")
            end
          end
        elseif string.lower(commands[1]) == "newlist" and commands[2] then
          commands[2] = tonumber(commands[2])
          if type(commands[2]) == "number" and words[2] <= #matches and commands[2] > 0 then
            txt = matches[commands[2]].id
            --generateList(txt)
            print(txt)
          end
        end
      end
    end
  end
end
