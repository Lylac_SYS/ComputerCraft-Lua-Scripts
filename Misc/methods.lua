local side = ...
local methods = peripheral.getMethods(side)
local m = peripheral.wrap("top")

m.clear()
m.setCursorPos(1,0)

for i = 1, #methods do
  m.setCursorPos(1,i)
  m.write(i.." = "..methods[i])
end
