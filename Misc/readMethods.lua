local tArgs = { ... }

if (#tArgs ~= 2) then
  error("Usage: "..shell.getRunningProgram().." <peripheral> <monitor>")
end

local side = tArgs[1]
local monitor = tArgs[2]
p = peripheral.wrap(side)
m = peripheral.wrap(monitor)
x = 1
y = 1

methods = peripheral.getMethods(side)
--methods = p.listMethods()

m.clear()
m.setCursorPos(1, 1)

--for i = 1, #methods, 1 do 
--  print(methods[i])
--end

for i = 1, #methods, 1 do 
  m.write(methods[i])
  y = y + 1
  m.setCursorPos(x, y)
  if (i == 19) then 
    x = x + 24
    y = 1
    m.setCursorPos(x, y)
  end
end
