F,L,R,B = "front","left","right","back"
E,W,N,S = "east","west","north","south"
a,b,c = 1,1,0 --Slot, Item, Flower

function p(x) return peripheral.wrap(x) end

function invSize(x) return p(x).getInventorySize() end

function slotData(x,i)
  if p(x).getStackInSlot(i)==nil then
    item = {qty=0}
	return item
  else
    return p(x).getStackInSlot(i)
  end
end

function chkInv()
  chstScore1 = 0  
  chstScore2 = 0  
  for i = 1, invSize(F) do
    if slotData(F,i)==nil then
      chstScore1 = chstScore+1
    elseif (slotData(B,i)~=nil) and (chstEmpty.qty==64) then
      chstScore2 = chstScore2+1
    elseif (slotData(F,i)~=nil) then
      a = i 
	  b = b+slotData(F,a).qty
      b = ((a*64)-slotData(F,a).qty)
      break
    end
  end
  if chstScore1==invSize(F) then
    print("Done")
    print(c," Flowers Planted")
    break
  elseif chstScore2==invSize(B) then
    print("Not Enough Space")
    break
  end
  print([[Slot      : ]],a,[[/]],invSize(F),[[ 
Item      : ]],(slotData(F,a).qty),[[/64 
Space Left: ]],(invSize(B)*64)-b,[[/]],(invSize(B)*64),[[ 
]],c,[[ flowers planted]])
end

while true do 
  term.clear()
  term.setCursorPos(1,1)
  chkInv()
  a = math.ceil((b+1)/64)
  c = c+1
  p(F).pushItemIntoSlot(N,a,1,1)
  p(L).pullItemIntoSlot(W,1,1,1)
  sleep(1)
  turtle.digDown()
  --turtle.craft(1)
  p(B).pullItemIntoSlot(S,1,1,a)
  b = b+1
end