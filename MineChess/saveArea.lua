function getFacing()
  directions = {"unknown","north","south","west","east"}
  blockInfo = commands.getBlockInfo(commands.getBlockPosition())
--  print("My facing is ", directions[blockInfo.metadata],".")
  facing = directions[blockInfo.metadata]
--  print(facing)
end
 
function setModifiers()
  fx,fz = 0,0
  if facing == "west" then fx,fz = 1,1
    elseif facing == "east" then fx,fz = -1,-1
    elseif facing == "north" then fx,fz = -1,1
    elseif facing == "south" then fx,fz = 1,-1
  end
end
 
function relXYZ(x,y,z)
  cx,cy,cz = commands.getBlockPosition()
--  print(cx+fx+(x*fx),",",cy+y,",",cz+fz+(z*fz))
--  commands.setblock(cx+fx+(x*fx),cy+y,cz+fz+(z*fz),"minecraft:stone")
  return (cx+fx+(x*fx)), (cy+y), (cz+fz+(z*fz))
end
 
function writeBlocks()
  file = io.open("blocks","w")
  file:write(textutils.serialize(blockData))
  file:close()
  print("Saving Complete!")
--  print(textutils.serialize(blockData))
end
 
function readBlocks()
  blockData = {}
  file = io.open("blocks","r")
  blockData = textutils.unserialize(file:read("*a"))
  file:close()
--  print(textutils.serialize(blockData))
end

function saveArea(x,y,z, x2,y2,z2)
  print("Saving Area, this may take a moment...")
  xStart, yStart, zStart = relXYZ(x,y,z)
  xEnd, yEnd, zEnd = relXYZ(x2,y2,z2)
  blockData = {}
  print(x,"x:",x2,"x2 ",y,"y:",y2,"y2 ",z,"z:",z2,"z2/fx:",fx," fz:",fz)
  for x = xStart, xEnd,fx do
    blockData[x] = {}
    for y = yStart, yEnd do
      blockData[x][y] = {}
      for z = zStart, zEnd,fz do
        blockData[x][y][z] = commands.getBlockInfo(x,y,z)
      end
    end
  end
  writeBlocks()
end
 
function revertArea(x,y,z, x2,y2,z2)
  print("Reverting Area...")
  readBlocks()
  xStart,yStart,zStart = relXYZ(x,y,z)
  xEnd,yEnd,zEnd = relXYZ(x2,y2,z2)
--  print(x,"x:",x2,"x2 ",y,"y:",y2,"y2 ",z,"z:",z2,"z2")
  for x = xStart, xEnd,fx do
    for y = yStart, yEnd do
      for z = zStart, zEnd,fz do
        commands.async.setblock(x,y,z,blockData[x][y][z]["name"], blockData[x][y][z]["metadata"])
      end
    end
  end
  print("Revert Complete!")
end
 
function clearArea(x,y,z, x2,y2,z2)
  print("Clearing Space...")
--  print(x,"x:",x2,"x2 ",y,"y:",y2,"y2 ",z,"z:",z2,"z2")
  xStart, yStart, zStart = relXYZ(x,y,z)
  xEnd, yEnd, zEnd = relXYZ(x2,y2,z2)
  for x = xStart, xEnd,fx do
    for y = yStart, yEnd do
      for z = zStart, zEnd,fz do
        commands.async.setblock(x,y,z,"minecraft:air")
      end
    end
  end
end
 
getFacing()
setModifiers()
