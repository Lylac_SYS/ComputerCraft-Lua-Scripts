--os.loadAPI("button")
os.loadAPI("saveArea")
--button.connectMonitors()
m = {}
--m = button.m
 
function checkFiles()
  file1 = io.open("saveArea","r")
  file2 = io.open("chess","r")
  if file1 and file2 then
    file1:close()
        file2:close()
    return true
  else
    --download both files
    return false
  end
end
 
function getPieces()
  P1Name = '"Tamtam18_2"'
  P2Name = '"sickhippie"'
--  commands.async.setblock(p,o,s,"minecraft:skull",9,"replace","{SkullType:3,"..P1Rot..",ExtraType:"..extraData[i].."}")
--  commands.async.setblock(p,o,s,"minecraft:skull",9,"replace","{SkullType:3,"..extraData[i].."}")
  extraData = {
    --Player 1 piece set
    [1]='P1Rot,ExtraType:"jeb_"',           --Rook
    [2]='P1Rot,ExtraType:"Dinnerbone"',     --Knight
    [3]='P1Rot,ExtraType:"C418"',           --Bishop
    [4]='P1Rot,ExtraType:"Notch"',          --Queen
    [5]='P1Rot,ExtraType:'..P1Name,             --King
    [6]='P1Rot,ExtraType:"C418"',           --Bishop
    [7]='P1Rot,ExtraType:"Dinnerbone"',     --Knight
    [8]='P1Rot,ExtraType:"jeb_"',           --Rook
 
    [9]='P1Rot,ExtraType:"MHF_Steve"',      --Pawns*8
    [10]='P1Rot,ExtraType:"MHF_Steve"',
    [11]='P1Rot,ExtraType:"MHF_Steve"',
    [12]='P1Rot,ExtraType:"MHF_Steve"',
    [13]='P1Rot,ExtraType:"MHF_Steve"',
    [14]='P1Rot,ExtraType:"MHF_Steve"',
    [15]='P1Rot,ExtraType:"MHF_Steve"',
    [16]='P1Rot,ExtraType:"MHF_Steve"',
    --Player 2 piece set
    [17]='P2Rot,ExtraType:"MHF_Zombie"',    --Pawns*8
    [18]='P2Rot,ExtraType:"MHF_Zombie"',
    [19]='P2Rot,ExtraType:"MHF_Zombie"',
    [20]='P2Rot,ExtraType:"MHF_Zombie"',
    [21]='P2Rot,ExtraType:"MHF_Zombie"',
    [22]='P2Rot,ExtraType:"MHF_Zombie"',
    [23]='P2Rot,ExtraType:"MHF_Zombie"',
    [24]='P2Rot,ExtraType:"MHF_Zombie"',
 
    [25]='P2Rot,ExtraType:"MHF_Skeleton"',  --Rook
    [26]='P2Rot,ExtraType:"MHF_Enderman"',  --Knight
    [27]='P2Rot,ExtraType:"MHF_Creeper"',   --Bishop
    [28]='P2Rot,ExtraType:'..P2Name,            --King
    [29]='P2Rot,ExtraType:"Herobrine"',     --Queen
    [30]='P2Rot,ExtraType:"MHF_Creeper"',   --Bishop
    [31]='P2Rot,ExtraType:"MHF_Enderman"',  --Knight
    [32]='P2Rot,ExtraType:"MHF_Skeleton"',  --Rook
  }
--  textutils.pagedPrint(textutils.serialize(extraData))
end
 
function mainMenu()
  button.clearTable()
  m.setBackgroundColor(colors.black)
  m.clear()
  m.setTextColor(colors.white)
  m.setCursorPos(1,1)
  button.label(11,1,"Welcome to MineChess")
  button.label(14,2,   "by Tamtam18_2")
  button.label(16,4,     "Credits:")
  button.label(15,5,    "Direwolf20")
  button.label(13,6,  "~ saveArea API")
  button.label(13,7,  "NitrogenFingers")
  button.label(13,8,  "~ Chess Engine")
    button.setTable("Reset Area", resetArea, "", 22, 37, 6, 8)
    button.setTable("Start Game", prompt, "", 10, 30, 12, 14)
  button.screen()
end
 
function prompt()
  button.flash("Start Game")
  button.clearTable()
  m.clear()
  m.setCursorPos(1,1)
  button.label(3,2,"Starting a new game will")
  button.label(3,3,"temporarily remove blocks in a")
  button.label(3,4,"10x10x4 Area starting at 1x,-1y,1z")
  button.label(3,5,"relative to facing the computer")
  button.label(3,6,"This will not save special")
  button.label(3,7,"blocks/tile entities.")
  m.setTextColor(colors.orange)
  button.label(7,10,"Remove any before continuing.")
  m.setTextColor(colors.white)
    button.setTable("continue",setupGame,"",10,30,15,17)
  button.screen()
end
 
function setModifiers()
  facing = saveArea.facing
  print("facing: ",facing)
  if facing == "west" then
    P1Rot = "Rot:4"
    P2Rot = "Rot:12"
  end
  if facing == "east" then
    P1Rot = "Rot:12"
    P2Rot = "Rot:4"
  end
  if facing == "north" then
    P1Rot = "Rot:8"
    P2Rot = "Rot:0"
  end
  if facing == "south" then
    P1Rot = "Rot:0"
    P2Rot = "Rot:8"
  end
end
 
function placePieces()
  getPieces()
  i = 1
  function setBlockPiece(x,z)
    p,o,s=saveArea.relXYZ(x,0,z)
--    textutils.pagedPrint(p..o..s)
--    commands.async.setblock(p,o,s,"minecraft:stone")
--    commands.async.setblock(p,o,s,"minecraft:skull",9,"replace","{SkullType:3,"..P1Rot..",ExtraType:"..extraData[i].."}")
    commands.async.setblock(p,o,s,"minecraft:skull",9,"replace","{SkullType:3,"..extraData[i].."}")
    print("x: ",x," z: ",z," i: ",i)
    i = i+1
  end
  print("facing: ",facing)
  for x = 1,8 do
    for z = 1,8 do
      if (facing == "north") or (facing == "south") then
        if z < 3 or z > 6 then
		  print("z: ",z)
		  setBlockPiece(x,z)
        end
      else
        if x < 3 or x > 6 then
		  print("x: ",x)
		  setBlockPiece(x,z)
        end
      end
    end
  end
  print("Playing Pieces Placed")
end
 
function placeBoard()
  white = false
  for x = 0,9 do
    for z = 0,9 do
      p,o,s=saveArea.relXYZ(x,-1,z)
      commands.async.setblock(p,o,s,"minecraft:stonebrick")
    end
  end
  for x = 1,8 do
    for z = 1,8 do
      p,o,s=saveArea.relXYZ(x,-1,z)
      if white then
        commands.async.setblock(p,o,s,"minecraft:wool", 0)
      else
        commands.async.setblock(p,o,s,"minecraft:wool", 15)
      end
      white = not white
    end
    white = not white
  end
  print("Playing Board Placed")
end
 
function setupGame()
--  button.flash("continue")
  m.clear()
  button.clearTable()
  m.setCursorPos(1,1)
  m.write("Setting up new game")
    saveArea.saveArea(0,-1,0,9,2,9)
    saveArea.clearArea(0,-1,0,9,2,9)
  m.setCursorPos(1,2)
  m.write("Placing Game Board")
    placeBoard()
  m.setCursorPos(1,3)
  m.write("Placing Pieces")
    placePieces()
  --resetArea()
  mainMenu()
end
 
function resetArea()
--  button.flash("Reset Area")
  saveArea.revertArea(0,-1,0,9,2,9)
end
 
function getClick()
  event, side, x, y = os.pullEvent("monitor_touch")
  return x,y,side
end
 
function wait()
  x,y = getClick()
  if button.checkxy(x,y) then return true end
end
 
setModifiers()
--mainMenu()
--setupGame()
resetArea()
placePieces()
--while true do wait() end