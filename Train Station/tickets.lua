local p = peripheral.wrap("top")
local t = peripheral.wrap("left")
local data = {}
rednet.open("right")

function writeFile()
   file = io.open("locs", "w")
   outp = textutils.serialize(data)
   file:write(outp)
   file:flush()
   file:close()
end

function readFile()
   file = io.open("locs", "r")
   if file == nil then writeFile() else
     inp = file:read()
     data = textutils.unserialize(inp)
   end
   file:close()
end

function printFile()
   file = io.open("locs", "r")
   print(file:read())
   file:close()
end

function addName(slot,name)
   data[slot] = name
   writeFile()
end

function removeSlot(slot)
   data[slot] = null
   writeFile()
end

function checkSlots(id)
  local slotData
  local invSize = p.getInventorySize()
  local i = 1
  
  print("Number of Slots: "..invSize)
  while true do
    slotData = p.getStackInSlot(i)
    if data[i] == nil and slotData == nil then
      i = i+1
    elseif data[i] ~= nil and slotData ~= nil then
      i = i+1
    elseif data[i] == nil and slotData ~= nil then
      print("Detected New Book in Slot "..i)
--      print("send "..i+1)
      rednet.send(id, "getName")
      local id, msg, dist = rednet.receive()
      addName(i, msg)
      i = i+1
    elseif data[i] ~= nil and slotData == nil then
      data[i] = null
      writeFile()
      break
    end
    if i == invSize then
      print("Done Checking Slots")
      printFile()
      break
    end
  end
  rednet.send(id,"done")
end

function book(slot,id)
  t.createTicket(tostring(data[slot]), 1)
  print(data[slot])
  t.pushItem("west", 3, 1, 1)
  turtle.dropDown(1)
  redstone.setOutput("bottom", true)
  sleep(1)
  redstone.setOutput("bottom", false)
  rednet.send(tonumber(id), "done")
end

function getBook()
   rs.setBundledOutput("back", 1)
   sleep(0.25)
   rs.setBundledOutput("back", 0)
   turtle.down()
   turtle.suck()
   turtle.up()
end

function getNames(id)
   local nameTbl = textutils.serialize(data)
   rednet.send(tonumber(id), nameTbl)
end

readFile()
printFile()

while true do
   local id, msg, dis = rednet.receive()
   local newmsg = string.match(msg, "%a+")
--   print(msg)
   if newmsg == "checkSlots" or redstone.getOutput("back") then
     checkSlots(id) 
   elseif newmsg == "getNames" then
     getNames(id)
   elseif newmsg == "remove" then
     removeSlot(tonumber(string.match(msg, "%d+")))
     rednet.send(id,"done")
   elseif newmsg == "books" then
     slot = string.match(msg, "%d+")
     book(tonumber(slot), id)
   end
end
