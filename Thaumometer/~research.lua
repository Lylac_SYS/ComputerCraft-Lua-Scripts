ae = peripheral.find("tileinterface")
dir = "south"

term.clear()
term.setCursorPos(1,1)
print("Press ctrl + T to end program at any time.")
print("Preparing to drop Items in:")
for i = 10, 1, -1 do 
  term.setCursorPos(1,4)
  term.write("[ "..i.." ] ")
  sleep(1)
end
term.setCursorPos(1,5)
rawInv = ae.getAvailableItems()
inv = {}
for i = 1, #rawInv do 
  inv[i] = {}
  inv[i].id = rawInv[i].fingerprint.id  
  inv[i].size = rawInv[i].size
  inv[i].dmg = rawInv[i].fingerprint.dmg
  assert(inv[i].id)
  assert(inv[i].size)
  if rawInv[i].fingerprint.nbt_hash then
    inv[i].nbt_hash = rawInv[i].fingerprint.nbt_hash
    assert(inv[i].nbt_hash)
  end
  ID = string.lower(inv[i].id)--..":"..inv[i].dmg)
  txt1 = "queen"
  txt2 = "headcrumbs"
  txt3 = "linkbook"
  txt4 = "chisel"
  txt5 = "facade"
  txt6 = "biomesoplenty"
  txt7 = "ic2"
  txt8 = "awwayoftime"
  txt9 = "draconicevolution"
  txt10 = "itemsoulvessel"
  txt11 = "twilightforest"
  if inv[i].size > 0 then
--    print("["..i.."]"..inv[i].id..":"..inv[i].dmg)
    if 
    not (string.find(ID, txt1) 
    or string.find(ID, txt2) 
    or string.find(ID, txt3) 
    or string.find(ID, txt4)
    or string.find(ID, txt5)
    or string.find(ID, txt6)
    or string.find(ID, txt7)
    or string.find(ID, txt8)
    or string.find(ID, txt9)
    or string.find(ID, txt10)
    or string.find(ID, txt11)) then
      ae.exportItem(inv[i], dir, 1)
      sleep(3)
      turtle.dropDown(1)
    end
  end
end
