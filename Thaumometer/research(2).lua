ae = peripheral.find("tileinterface")
dir = "south" --Direction of the interface facing the turtle. Can also use "up" and "down"
filter = true --Default Setting, true = quick
speed = 2     --rate of items being dropped
arg = {...}   --research <full>
 
if (#arg==1 and arg[1]~="full") then
  error("Usage: "..shell.getRunningProgram().." <full or quick>")
else if arg[1]=="full" then filter = not filter end end
 
function start()
  term.clear()
  term.setCursorPos(1,1)
  print("Press ctrl + T to end program at any time.")
  print("Preparing to drop Items in:")
  for i=7,1,-1 do 
    term.setCursorPos(1,4)
    term.write("[ "..i.." ] ")
    sleep(1)
  end
  term.setCursorPos(1,5)
end

function filterItems()
--The list of strings the filter looks for.
--If there is a match, the code will skip that item.
--Items can be filtered by mod.
--use lowercase when adding a filter
  itemFilter = {}
  if filter==true then
    itemFilter = {
    "queen",--re-disable if errors ocurr
    "headcrumbs",
    "linkbook",
    "chisel",
    "facade",
    "biomesoplenty",
    "ic2",
    "awwayoftime",
    "draconicevolution",
    "itemsoulvessel",
    "twilightforest",
    }
  end
end

function dropItems()
  filterItems()
--  textutils.pagedPrint(textutils.serialize(itemFilter))
  rawInv = ae.getAvailableItems()
  inv = {}
  item = true
  for i = 1, #rawInv do
    inv[i] = {}
    inv[i].id = rawInv[i].fingerprint.id
    ID = string.lower(tostring(inv[i].id))
    inv[i].size = rawInv[i].size
    inv[i].dmg = rawInv[i].fingerprint.dmg
    assert(inv[i].id)
    assert(inv[i].size)
    if rawInv[i].fingerprint.nbt_hash then
      inv[i].nbt_hash = rawInv[i].fingerprint.nbt_hash
      assert(inv[i].nbt_hash)
    end
    print("["..i.."]"..inv[i].id..":"..inv[i].dmg)
    if filter == true then
      for j = 1, #itemFilter do 
        if not (string.find(ID, string.lower(itemFilter[j]))) then
          item = true
        else
          item = false
          break
        end
      end
    end
    if (inv[i].size > 0 and item==true) then
      ae.exportItem(inv[i],dir,1)
      sleep(speed)
      turtle.dropDown(1)
    end
  end
end
 
start()
dropItems()
