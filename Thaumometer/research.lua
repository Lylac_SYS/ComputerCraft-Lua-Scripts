ae = peripheral.find("tileinterface")
dir = "south" --Direction of the interface facing the turtle
filter = true
arg = {...}
 
if (#arg~=1 and arg[1]~=(full or quick)) then 
  error("Usage: "),shell.getRunningProgram()," <full or quick>")
else if arg[1]==full then filter = false end end
 
function start()
  term.clear()
  term.setCursorPos(1,1)
  print("Press ctrl + T to end program at any time.")
  print("Preparing to drop Items in:")
  for i=7,1,-1 do
    term.setCursorPos(1,4)
    term.write("[ "..i.." ] ")
    sleep(1)
  end
  term.setCursorPos(1,5)
end

function filterItems()
  itemFilter = {}
  if filter==true then
    itemFilter = {
	"queen",
    "headcrumbs",
    "linkbook",
    "chisel",
    "facade",
    "biomesoplenty",
    "ic2",
    "awwayoftime",
    "draconicevolution",
    "itemsoulvessel",
    "twilightforest",
    }
  end
end

function dropItems()
  filterItems()
  rawInv = ae.getAvailableItems()
  inv = {}
  item =  true
  for i = 1, #rawInv do
    inv[i] = {}
    inv[i].id = rawInv[i].fingerprint.id
	ID = string.lower(inv[i].id)
    inv[i].size = rawInv[i].size
    inv[i].dmg = rawInv[i].fingerprint.dmg
    assert(inv[i].id)
    assert(inv[i].size)
    if rawInv[i].fingerprint.nbt_hash then
      inv[i].nbt_hash = rawInv[i].fingerprint.nbt_hash
      assert(inv[i].nbt_hash)
    end
--    print("["..i.."]"..inv[i].id..":"..inv[i].dmg)
	if filter == true then
	  print("Filter: true")
      for j = 1, #itemFilter do 
	    if not (string.find(ID, itemFilter[j]) then 
	      item = true else item = false
	    end
	  end
	  print("Item: ",item)
	end
    if (inv[i].size > 0 and item==true) then
      ae.exportItem(inv[i],dir,1)
      sleep(3)
      turtle.dropDown(1)
    end
  end
end
 
start()
dropItems()